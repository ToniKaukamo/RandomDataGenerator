//Scripts.js
const nameGen = () => {
    // Generates from list to complete a full name
    const randomFirstName = firstNames[Math.floor(Math.random() * firstNames.length)];
    const randomSurname = lastNames[Math.floor(Math.random() * lastNames.length)];
    return `${randomFirstName} ${randomSurname}`;
};

const phoneGen = () => {
    // Mobile telephone number based on these two links
    // http://www.kielitoimistonohjepankki.fi/haku/puhelinnumerot/ohje/33
    // https://www.traficom.fi/en/communications/broadband-and-telephone/mobile-network-area-codes
    const rareNumber = Math.random() < 1 / 20;
    let startNum = '';
    if (rareNumber) {
        startNum = "050";
    } else {
        startNum = "04" + Math.floor(Math.random() * 10);
    };
    const middleNum = '' + Math.floor(Math.random() * 10) + Math.floor(Math.random() * 10) + Math.floor(Math.random() * 10);
    const endNum = '' + Math.floor(Math.random() * 10) + Math.floor(Math.random() * 10) + Math.floor(Math.random() * 10) + Math.floor(Math.random() * 10);
    return `${startNum} ${middleNum}${endNum}`;
};

const addressGen=()=>{
    // Notes Finland Postal code in range of 00100 - 99990
    // Detailed address in form of houseNumber + address + postal code (Example: 2A Punkkerikatu,53850)
    const postalCode = Math.floor(Math.random() * (99990 - 100 + 1) + 100);
    const formatedPC = postalCode.toString().padStart(5,"0");
    const houseNumber = Math.floor(Math.random() * 26 + 65);
    const detailedAddress = Math.floor(Math.random() * 9 + 1) + String.fromCharCode(houseNumber) + " " + addresses[Math.floor(Math.random() * addresses.length)] + 
    ", " + formatedPC;
    return detailedAddress;
};

/**
 * 
 * @param {String} rName Firstname Lastname
 * @returns 
 */
const emailGen = (rName=undefined) => {
    // Implement variety in names -> ExampleName02
    let name = undefined;
    if (rName != undefined) {
        name = rName.replace(' ', '.');
    }else {
        name = nameGen().replace(' ', '.');
    }
    const randomDomain = emailDomains[Math.floor(Math.random() * emailDomains.length)];
    return `${name}@${randomDomain}`
};

const finnishSSNGen = () => {
    const generateDate = () => {
        const currentYear = new Date().getFullYear();
        const maxAge = 90;
        const year = Math.floor(Math.random()* (currentYear - (currentYear-maxAge) + 1)) + (currentYear-maxAge);
        const month = Math.floor(Math.random()*12) + 1;
        const day = Math.floor(Math.random()* 28) + 1;
        const formatMonth = month < 10 ? `0${month}` : `${month}`;
        const formatDay = day < 10 ? `0${day}` : `${day}`;
        const formatYear = year.toString().slice(2,4);
        const finalDate = `${formatDay}${formatMonth}${formatYear}`;// 110901
        return { year, finalDate }; 
    }
    const {year, finalDate} = generateDate();
    //Using year to get the correct letter
    var yearSuffix = { "20" : 'A', "19" : '-' }
    let letterDate = yearSuffix[year.toString().slice(0,2)];
    let personalNum = Math.floor(Math.random() * ((899 - 2) / 2) + 2) * 2 + 1;
    let leadZero = personalNum.toString();
    //Generate 0 if needed
    while (leadZero.length < 3) leadZero = '0' + leadZero;
    //Check if the number and add the ending letter
    let combined = finalDate + leadZero;
    let checkIndex = parseInt(combined) % 31;
    let personalIDC = `${finalDate}${letterDate}${leadZero}${checkNum[checkIndex]}`;
    return `${personalIDC}`;
}
/**
 * 
 * @param {Number} rowAmount Number of rows generated
 * @param {List} wanted options[name,phone,email]
 * @returns Formatted JSON object with indent size 4
 */
const writeJson = (rowAmount,wanted) => {
    dataPoints = [];
    for (let i = 0; i < rowAmount; i++) {
        const dataPoint = {};
        if (wanted.includes("name")) {
            dataPoint.name = nameGen();
        }
        if (wanted.includes("phone")) {
            dataPoint.phone = phoneGen();
        }
        if (wanted.includes("email")) {
            if(wanted.includes("name")) {
                dataPoint.email = emailGen(dataPoint.name);
            } else {
                dataPoint.email = emailGen();
            }
        }
        if (wanted.includes("address")) {
            dataPoint.address = addressGen();
        }
        if (wanted.includes("SSN")) {
            dataPoint.SSN = finnishSSNGen();
        }
        dataPoints.push(dataPoint);
    }
    return JSON.stringify(dataPoints,undefined,4);
};
/**
 * 
 * @param {Number} rowAmount Number of rows generated
 * @param {List} wanted options[name,phone,email]
 * @returns CSV object
 */
const writeCSV = (rowAmount,wanted) => {
    const csvHeader = wanted;
    const csvLines = [csvHeader.join(",")];
    for (let i = 0; i < rowAmount; i++) {
        const dataPoint = [];
        let name;
        if (wanted.includes("name")) {
            name = nameGen();
            dataPoint.push(name);
        }
        if (wanted.includes("phone")) {
            dataPoint.push(phoneGen());
        }
        if (wanted.includes("email")) {
            let email;
            if (wanted.includes("name")) {
                email = emailGen(name);
            } else {
                email = emailGen();
            }
            dataPoint.push(email);
        }
        if (wanted.includes("address")) {
            dataPoint.push(addressGen());
        }
        if (wanted.includes("SSN")) {
            dataPoint.SSN = finnishSSNGen();
        }
        csvLines.push(dataPoint.join(","));
    }
    return csvLines.join('\n');
}

const getInputs = () => {
    const checked = [];
    const checkedBoxes = document.querySelectorAll('input[type="checkbox"]:checked');
    for (let checkbox of checkedBoxes) {  
        checked.push(checkbox.value);  
    }
    const format = document.querySelector('input[name="format"]:checked').value;
    const rowAmount = document.getElementById('amount').value;
    return {rowAmount,format,checked};
}

const showInstructions = () => {
    alert("Welcome to the Random Finnish Data Generator! With this tool, you can choose what type of data you want to create and the format type of data file. If you want to copy to your clipboard, use the Copy button. Thank you!");
} 

const copyToClipboard = () => {
    const results = document.getElementById("result");
    const textArea = document.createElement("textarea");
    textArea.value = results.textContent;
    document.body.appendChild(textArea);
    textArea.select();
    document.execCommand("Copy");
    document.body.removeChild(textArea);

    alert("Copied to clipboard!");
};

const main = () => {
    const { rowAmount, format, checked } = getInputs();
    const results = document.getElementById("result");
    const copyButton = document.getElementById("copyButton");

    if (format === "json") {
        results.innerHTML = writeJson(rowAmount, checked);
    } else if (format === "csv") {
        results.innerHTML = writeCSV(rowAmount, checked);
    }
    // Show the copy button
    copyButton.style.display = "block";
    // Change the 'onclick' event to use the copyToClipboard function
    copyButton.onclick = copyToClipboard;
}

const getContents = () => {
    const content = document.getElementById("result").textContent;
    return content;
}

const generateFile = (content) => {
    let fileName = "generated_data";
    if (content[0] !== "[") {
        //Generate csv file
        var blob = new Blob([content], {type: "text/csv"});
        fileName += ".csv";
    } else {
        //Generate json file
        var blob = new Blob([content], {type: "application/json"});
        fileName += ".json";
    }
    return {blob, fileName}
}
const downloadFile = () => {
    const {blob, fileName} = generateFile(getContents());
    const downloadLink = document.createElement('a'); // Create the download link element
    downloadLink.href = window.URL.createObjectURL(blob); // Add the link to element
    downloadLink.download = fileName; // Add default filename 
    document.body.appendChild(downloadLink); // Append the download element to website
    downloadLink.click(); // Click element
    document.body.removeChild(downloadLink); // Remove element
}

// Most common names, surnames and domains
const firstNames = [
    "Aino", "Akseli", "Aleksi", "Anni", "Eero", "Elina", "Elias", "Ella", "Emma", "Ilari",
    "Iida", "Kaisa", "Lauri", "Maria", "Matias", "Onni", "Riikka", "Sofia", "Topi", "Venla"
];
const lastNames = [
    "Virtanen", "Korhonen", "Mäkinen", "Nieminen", "Koskinen", "Heikkinen", "Järvinen",
    "Mäkelä", "Hämäläinen", "Laine", "Lehtonen", "Lindholm", "Peltola", "Jokinen", "Laaksonen",
    "Salmi", "Lahti", "Rantanen", "Kallio", "Kärkkäinen"
];
const emailDomains = [
    "gmail.com", "yahoo.com", "outlook.com", "hotmail.com"
];

// Adresses
const addresses = [
    "Punkkerikatu", "Villimiehenkatu", "Linnunrata", "Puhakankatu", "Piliuvankatu", "Teknologiapuistonkatu",
    "Leirikatu", "Valtakatu", "Keskuskatu", "Lavolankatu", "Ajurinkatu", "Merenlahdentie", "Skinnarilankatu",
    "Helsingintie", "Toikankatu", "Torkkelinkatu", "Hallituskatu", "Koskikatu", "Ratapihantie", "Teollisuuskatu"
];

const checkNum = {
    0 : "0",
    1 : "1",
    2 :	"2",
    3 :	"3",
    4 :	"4",
    5 :	"5",
    6 :	"6",
    7 :	"7",
    8 :	"8",
    9 :	"9",
    10 : "A",
    11 : "B",
    12 : "C",
    13 : "D",
    14 : "E",
    15 : "F",
    16 : "H",
    17 : "J",
    18 : "K",
    19 : "L",
    20 : "M",
    21 : "N",
    22 : "P",
    23 : "R",
    24 : "S",
    25 : "T",
    26 : "U",
    27 : "V",
    28 : "W",
    29 : "X",
    30 : "Y"
}

module.exports = {
    addressGen,
    nameGen,
    emailGen,
    phoneGen,
    finnishSSNGen,
    writeJson,
    writeCSV,
    getInputs,
    downloadFile,
    generateFile,
    getContents,
}