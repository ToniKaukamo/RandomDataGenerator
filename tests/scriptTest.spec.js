const expect = require("chai").expect;
const scripts = require("../scripts");

describe("Testing the addressGen function",()=>{
    before(()=>{
        console.log("Starting the tests");
    });

    after(()=>{
        console.log("All tests completed");
    });

    it("Test1: Should generate appropriate address",()=>{
        const detailedAddress = scripts.addressGen();

        //Checking detailedAddress is a string
        expect(detailedAddress).to.be.a("string");

        const [syntax,address,PC] = detailedAddress.split(" ");
        
        //console.log(syntax)
        //console.log(address)
        //console.log(PC)

        //Checking house number syntax
        const [num,char] = syntax.split("");

        //Checking the number in house number smaller  10
        const checkNum = (num)=>{
            return 0<parseInt(num) && parseInt(num)<10;
        };
        expect(checkNum(num)).to.be.true;
        
        //Checking the character in house number is in uppercase
        const checkChar = (char)=>{
            const dec = char.charCodeAt(0);
            let bool = true;
            if (dec <65 && dec <90){
                bool = false;
            }else{
                bool = true;
            }
            return bool;
        }
        expect(checkChar(char)).to.be.true;
       
        //Checking address syntax and type
        const addressRegex = /^[A-Z][a-z]+,$/
        expect(address).to.match(addressRegex);

        //Checking the validity of Postal Code 
        //Checking lengh
        expect(PC.length).to.equal(5);
        // Checking value
        const checkPC = (PC)=>{
            return 100<parseInt(PC) && parseInt(PC)<99991;
        };
        expect(checkPC(PC)).to.be.true;

    });
});

describe("Testing scripts", () => {
  before(() => {
    console.log("Starting tests");
  });

  after(() => {
    console.log("All tests completed");
  });

  it("Test 1: Should generate appropriate name", () => {
    const Fullname = scripts.nameGen();

    // Checking Fullname is a string
    expect(Fullname).to.be.a("string");

    const [firstName, surName] = Fullname.split("");

    // Checking firstName is string and not empty
    expect(firstName).to.be.a("string").and.not.to.be.empty;

    // Checking surName is string and not empty
    expect(surName).to.be.a("string").and.not.to.be.empty;
  });

  it("Test2: Should generate appropriate phone number", () => {
    const phoneNum = scripts.phoneGen();
    const r_phoneNum = phoneNum.replace(" ", "");

    // checking phone number length
    expect(r_phoneNum.length).to.be.equal(10);

    // Checking phone regex
    const phoneRegex = /^(050|04\d)\s\d{3}\d{4}$/;
    expect(phoneNum).to.match(phoneRegex);
  });

  it("Test3: Should generate appropriate email", () => {
    // Case 1: Test with given name
    const name1 = "Khoi Do";
    const emailName1 = name1.replace(" ", ".");
    const email1 = scripts.emailGen(name1);

    // Checking email regex
    const emailRegex1 = new RegExp(
      `^${emailName1}@(gmail|yahoo|outlook|hotmail)\.com$`
    );
    expect(email1).to.match(emailRegex1);

    // // Case 2: Test with no given name:
    const name2 = undefined;
    const email2 = scripts.emailGen(name2);

    // Checking mail regex
    const emailRegex2 =
      /^[a-zA-ZäöåÄÖÅ]+\.[a-zA-ZäöåÄÖÅ]+@(gmail|yahoo|outlook|hotmail)\.com$/;
    expect(email2).to.match(emailRegex2);
  });

  it("Test 4: Should write as Json values", () => {
    const rowAmount = 5;
    const wanted = ["name", "phone", "email"];
    const Json = scripts.writeJson(rowAmount, wanted);

    // Parse Json string
    const JsonObject = JSON.parse(Json);

    // Checking array and length
    expect(JsonObject).to.be.an("array").with.lengthOf(rowAmount);

    for (const dataPoint of JsonObject) {
      // Checking each dataPoint has expected properties
      expect(dataPoint).to.have.keys("name", "phone", "email");

      // Checking name is string
      if (wanted.includes("name")) {
        expect(dataPoint.name).to.be.a("string");
      }

      // Checking phone regex is valid
      if (wanted.includes("phone")) {
        const phoneRegex = /^(050|04\d)\s\d{3}\d{4}$/;
        expect(dataPoint.phone).to.match(phoneRegex);
      }

      // Checking email regex is valid
      if (wanted.includes("email")) {
        const emailRegex = new RegExp(
          `^[a-zA-ZäöåÄÖÅ]+\.[a-zA-ZäöåÄÖÅ]+@(gmail|yahoo|outlook|hotmail)\.com$`
        );
        expect(dataPoint.email).to.match(emailRegex);
      }

      // Checking space indentation
      const jsonString = JSON.stringify(JsonObject, undefined, 4);
      expect(Json).to.equal(jsonString);
    }
  });

  it("Test 5: Should write as CSV values", () => {
    const rowAmount = 5;
    const wanted = ["name", "phone", "email"];
    const csv = scripts.writeCSV(rowAmount, wanted);

    // Checking array
    expect(csv).to.be.a("string");

    // Checking length (+1 due to the example line)
    const splitCSV = csv.split("\n").slice(1);
    expect(splitCSV).to.have.lengthOf(rowAmount);

    // Checking CSV syntax
    for (const dataPoint of splitCSV) {
      const combinedRegex =
        /^([a-zA-ZäöåÄÖÅ]+\s[a-zA-ZäöåÄÖÅ]+),((050|04\d)\s\d{3}\d{4}),([a-zA-ZäöåÄÖÅ]+\.[a-zA-ZäöåÄÖÅ]+@(gmail|yahoo|outlook|hotmail)\.com)$/;
      expect(dataPoint).to.match(combinedRegex);
    }  
  });
  it("Test 6: Should validate SSN", () => {
      // Replace this with your actual test data
      const inputSSN = scripts.finnishSSNGen();
      // Doesn't know if month has 31 or 30 days.
      const dateRegex = /^(0[1-9]|1[0-9]|2[0-9]|3[0-1])(0[1-9]|1[0-2])(0[1-9]|[0-9][0-9])$/; // Cases 01-31 01-12 00-99
      const birthDate = inputSSN.slice(0, 6);
      const personalNum = inputSSN.slice(7, 10);
      const checkNum = {
        0 : "0",
        1 : "1",
        2 :	"2",
        3 :	"3",
        4 :	"4",
        5 :	"5",
        6 :	"6",
        7 :	"7",
        8 :	"8",
        9 :	"9",
        10 : "A",
        11 : "B",
        12 : "C",
        13 : "D",
        14 : "E",
        15 : "F",
        16 : "H",
        17 : "J",
        18 : "K",
        19 : "L",
        20 : "M",
        21 : "N",
        22 : "P",
        23 : "R",
        24 : "S",
        25 : "T",
        26 : "U",
        27 : "V",
        28 : "W",
        29 : "X",
        30 : "Y"
      }; 
      const combined = birthDate + personalNum;
      const remainder = parseInt(combined) % 31;
      expect(dateRegex.test(birthDate)).to.equal(true);
      expect(checkNum[remainder]).to.equal(inputSSN.slice(10, 11));
  });
});

describe("File generation test", () => {
  it("Should generate csv file when given content is not json format", () => {
    const rowAmount = 10;
    const content = ["name", "phone", "email"];
    const csvContent = scripts.writeCSV(rowAmount, content);
    const {fileName, fileBlob} = scripts.generateFile(csvContent);
    expect(fileName.split(".")[1]).to.equal("csv");
  });
  it("Should generate json file when given content is json format", () => {
    const rowAmount = 5;
    const content = ["name", "phone", "email"];
    const jsonContent = scripts.writeJson(rowAmount, content);
    const {fileName, fileBlob} = scripts.generateFile(jsonContent);
    expect(fileName.split(".")[1]).to.equal("json");
  });
});

const { copyToClipboard, displayCopyAlert } = require('../scripts');

describe.skip('Clipboard Button Functionality', () => {
    it('copyToClipboard manipulates DOM as expected', () => {
        // Spy on the global object to capture the alert function
        const alertSpy = [];
        const originalAlert = global.alert;
        global.alert = (...args) => {
            alertSpy.push(args);
        };

        // Call the function to be tested
        copyToClipboard();

        // Check if displayCopyAlert was called
        expect(alertSpy.length).to.equal(1);
        expect(alertSpy[0][0]).to.equal('Copied to clipboard!');

        // Restore the original alert function
        global.alert = originalAlert;
    });
});
